#pragma once
#include "Sequence.h"

class String : public Sequence 
{
public:
	
	//constractor
	String(std::string str);
	
	//methods
	bool const isPrintable();

	std::string const toString();

private:
	std::string _value;
};