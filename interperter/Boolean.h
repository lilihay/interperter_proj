#pragma once
#include "Type.h"

class Boolean : public Type
{
public:

	//constractor
	Boolean(bool boolean);

	//methods
	bool const isPrintable();
	
	std::string const toString();

private:
	bool _value;
};