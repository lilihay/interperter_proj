#pragma once
#include <iostream>
#include <string>
#include "Helper.h"

class Type
{
public: 
	//constractors
	Type();

	//getters
	const bool getIsTemp();

	//setters
	void setIsTemp(const bool isTemp);

	//methods
	virtual const bool isPrintable() = 0;

	virtual const std::string toString() = 0;

protected:
	bool _isTemp = false;
};
