#include "Type.h"
#include "InterpreterException.h"
#include "Parser.h"
#include "IndentationException.h"
#include "SyntaxException.h"
#include <iostream>

#define WELCOME "Welcome to Magshimim Python Interperter version 1.0 by "
#define YOUR_NAME "Lihay Swissa"


int main(int argc,char **argv)
{
	std::cout << WELCOME << YOUR_NAME << std::endl;

	std::string input_string;

	// get new command from user
	std::cout << ">>> ";
	std::getline(std::cin, input_string);
	
	while (input_string != "quit()")
	{
		try
		{
			// parsing command
			Type* var = Parser::parseString(input_string);
			
			if (var->isPrintable())
			{
				std::cout << var->toString() << std::endl;
			}

			if (var->getIsTemp() == true)
			{
				delete var;
			}
		}
		catch(IndentationException InEx)
		{
			std::cout << InEx.what() << std::endl;
		}
		catch (syntaxExeption synEx)
		{
			std::cout << synEx.what() << std::endl;
		}


		// get new command from user
		std::cout << ">>> ";
		std::getline(std::cin, input_string);
	}

	return 0;
}
