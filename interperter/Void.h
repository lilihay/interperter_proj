#pragma once
#include "Type.h"

class Void : Type
{
public:

	//constractor
	Void();

	//methods
	bool const isPrintable();

	std::string const toString();

};