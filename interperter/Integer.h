#pragma once
#include "Type.h"

class Integer : public Type
{
public:

	//constractor
	Integer(int integer);

	//methods
	bool const isPrintable();

	std::string const toString();

private:
	int _value;
};