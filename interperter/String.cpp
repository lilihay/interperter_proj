#include "String.h"

String::String(std::string str) : _value(str) {}

bool const String::isPrintable()
{
    return true;
}

std::string const String::toString()
{
    return this->_value;
}
