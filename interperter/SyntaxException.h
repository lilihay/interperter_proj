#pragma once

#include "InterpreterException.h"

class syntaxExeption : InterpreterException
{
public:
	std::string what();
};