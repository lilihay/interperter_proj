#include "Parser.h"
#include "IndentationException.h"
#include "Boolean.h"
#include "Integer.h"
#include "String.h"
#include "Sequence.h"
#include "SyntaxException.h"
#include <iostream>

Type* Parser::parseString(std::string str)
{
	IndentationException InExeption;
	syntaxExeption SynExeption;

	if (!isspace(str[0]))
	{
		Helper::rtrim(str);

		Type* var = getType(str);

		if (var != nullptr)
		{
			return var;
		}

		else
		{
			throw SynExeption;
		}
	}

	else
	{
		throw InExeption;
	}
}

Type* Parser::getType(std::string& str)
{
	Helper::trim(str);

	if(Helper::isInteger(str))
	{
		Helper::removeLeadingZeros(str);
		
		Type* integer = new Integer (std::atoi(str.c_str()));
		integer->setIsTemp(true);

		return integer;
	}

	else if (Helper::isBoolean(str))
	{
		bool value = false;
		
		if (str == "True")
		{
			value = true;
		}

		Type* boolean = new Boolean(value);
		boolean->setIsTemp(true);

		return boolean;
	}

	else if (Helper::isString(str))
	{
		Type* stringType = new String(str);
		stringType->setIsTemp(true);
		
		return stringType;
	}

	else
	{
		return nullptr;
	}
}


