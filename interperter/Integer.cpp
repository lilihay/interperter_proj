#include "Integer.h"

Integer::Integer(int integer) : _value(integer) {}

bool const Integer::isPrintable()
{
	return true;
}

std::string const Integer::toString()
{
	return std::to_string(this->_value);
}
