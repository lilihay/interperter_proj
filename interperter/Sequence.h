#pragma once
#include "Type.h"

//template <class T>

class Sequence: public Type
{
public:

	//constractor
	Sequence();

	//methods
	bool const isPrintable() = 0;

	std::string const toString() = 0;

};
