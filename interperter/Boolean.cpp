#include "Boolean.h"

Boolean::Boolean(bool boolean) : _value(boolean) {}

const bool Boolean::isPrintable()
{
	return true;
}

const std::string Boolean::toString()
{
	return this->_value ? "True" : "False";
}
